
# `Acompañame`

## `Contribuidoras`

- Milagros Olazábal
- Javier Seiglie
- Williams Padilla
- Solange Pariona
- Caterine Ini Ambrogio

## `Respuesta al reto`

### `¿Qué es y como funciona?`

Aplicación "Acompañame", desarrollada para Android. Software de acompañamiento y seguimiento de mujeres y personas del colectivo LGBTIQ+ en situación de vulnerabilidad. Las alertas de seguridad ciudadana que se dan a través de esta app llegan a las dependencias policiales, usuarios de la aplicación que se encuentren dentro de un radio de 2km. La app hace uso de la geolocalización de nuestro teléfono para informar la ubicación exacta del incidente.

### `Estudio de mercado`

Para centrar nuestro desarrollo en una experiencia personalizada, y poder dedicarse así a las necesidades específicas de los y las usuarias, realizamos un breve formulario de 10 preguntas.

De cara a conocer más a las personas que podrían utilizar esta app compartimos una encuesta con 10 preguntas mediante redes sociales. La encuesta estuvo abierta durante una hora y tuvo como finalidad recopilar información sobre situaciones de acoso en espacios públicos, qué acciones se toman ante estos(tanto personal o a terceros).

Los resultados obtenidos luego de encuestar a 56 personas dejaron en claro una realidad compartida alarmante.

### `Objetivos`

Construir/Diseñar una aplicación que utilice Geoposicionamiento para denunciar zonas inseguras o de riesgos (calles, restaurantes, supermercados, etc), que involucren a mujeres y a la comunidad LGBTIQ+. Se considera una mejora continua, centrada en los y las usuarias como principales fuentes de información gracias a sus constantes feedbacks.(Diseño centrado en el usuario)

## `¿Cómo empezar?`

Primero, necesitas clonar el repositorio

    git clone git@gitlab.com:girlsintechspain_/hackathon2022/Equipo-1.git

luego acceder al directorio

    cd Equipo-1/
    code .

En el directorio del proyecto puedes correr lo siguientes comandos:

    npm install

Se intalará los nodemodules

    npm run start

Arranca el proyecto en modo desarrollo.\
abre [http://localhost:3000](http://localhost:3000) para verlo en tu navegador.


## `Enlaces`

Presentación

[https://drive.google.com/file/d/1_OPXQWXhL8IAyZMGjNN7m7ybJC51OoZS/view?usp=sharing](https://drive.google.com/file/d/1_OPXQWXhL8IAyZMGjNN7m7ybJC51OoZS/view?usp=sharing)

Mira nuestro video demo:

[https://res.cloudinary.com/willykronara/video/upload/v1654090888/Hackathon_prototipo_-_H4H_Grupo_1_uf4q5i.mp4](https://res.cloudinary.com/willykronara/video/upload/v1654090888/Hackathon_prototipo_-_H4H_Grupo_1_uf4q5i.mp4)

Presentación de producto

[https://docs.google.com/document/d/1KAjdEBBx9G1xT5XsydZPHKk_EaeApS5oCI850AzLeh4/edit?usp=sharing](https://docs.google.com/document/d/1KAjdEBBx9G1xT5XsydZPHKk_EaeApS5oCI850AzLeh4/edit?usp=sharing)

Mapa de calor

[https://europeanvalley.es/ia/maps/map.html](https://europeanvalley.es/ia/maps/map.html)

## `Tecnologías utilizadas`

### `Datos oficiales`

- Policía Municipal. Datos estadísticos actuaciones Policía Municipal
- Portal Estadístico. Delegación del Gobierno contra la Violencia de Género
- Ministerio del Interior. Estadística del Sistema de Seguimiento Integral en los Casos de Violencia de Género (Sistema VioGén) 2020. Anuario Estadístico del Ministerio del Interior
- Informe Sobre La Evolución De Los Delitos De Odio En España - Oficina Nacional De Lucha Contra Los Delitos De Odio - Ministerio Del Interior

En el ejemplo realizado los datos repartidos por distrito en la ciudad de Madrid , se ha realizado una acumulación de datos de los últimos 12 meses sobre el número de crímenes. Luego de acumular los datos se realiza un mapa de calor con cada punto geolocalizado. Teniendo en cuenta que la densidad poblacional es esencial para medir la criminalidad, ya que el porcentaje no es igual en poblaciones con 100 o 1000 personas, en caso de tener por ejemplo 20 crímenes al mes cada población. Por este motivo se buscó la densidad de población por distrito, y se equipara la criminalidad en función a la densidad de población. Por lo que el mapa de calor muestra un porcentaje real.

### `Desarrollo`

Se han utilizado las siguientes tecnologías:

- Python
- Librerías Folium
- React
- Figma
- OpenStreetMaps

## `Licencias`

Este proyecto utiliza licencia MIT.
