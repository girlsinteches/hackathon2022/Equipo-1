import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Success } from "./pages/Success/Success";
import { Register } from "./pages/register/Register";
import { HeatMap } from "./pages/map/map";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <BrowserRouter>
        <Routes>
            <Route path="/" element={<App />}/>
            <Route path="/success" element={<Success />}/>
            <Route path="/register" element={<Register />}/>
            <Route path="/map" element={<HeatMap />}/>
        </Routes>
    </BrowserRouter>
);
