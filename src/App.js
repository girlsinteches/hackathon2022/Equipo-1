import { useState } from "react";
import { Alert } from "./pages/Alert/alert";
import { PositionContext } from "./context/PositionContext";

function App() {
    const [positionContext, setPositionContext] = useState([]);

    return (
        <div className="App">
            <PositionContext.Provider
                value={{ positionContext, setPositionContext }}
            >
                <Alert />
            </PositionContext.Provider>
        </div>
    );
}

export default App;
