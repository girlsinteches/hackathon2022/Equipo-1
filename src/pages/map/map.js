import React from "react";

export const HeatMap = () => {
    return (
        <section>
            <h2>Mapa de calor</h2> {/* Color rojo */}
            <div className="iframe_holder">
                <iframe
                    src="https://europeanvalley.es/ia/maps/map.html"
                    width="1000"
                    height="600"
                >
                    Sorry your browser does not support inline frames.
                </iframe>
            </div>
            <div className="latest">
                <p>Últimas noticias: {}</p>
                <p></p>
            </div>
        </section>
    );
};

 
