import React, { useState } from "react";
import { Navbar } from "../../navbar/navbar";
import CHECK from "../../media/check.png";
import HAND from "../../media/hand.png"
import { useNavigate } from "react-router-dom";

export const Success = () => {
    const navigate = useNavigate();
    const [close, setClose] = useState(false);
    const [active, setActive] = useState({
        first: true,
        second: false,
        third: false
    });

    return (
        <main className={`success__container ${close && "closed"}`}>
            <Navbar
                closed={() => {
                    setClose(!close);
                    navigate("/");
                }}
            />

            <article className={`success__page success_one ${active.first && "active"}`}>
                <header className="success__send_container">
                    <img src={CHECK} alt="" className="success__img" />
                    <h1 className="success__title">Alerta enviada</h1>
                    <p className="success__subtitle">
                        Tu ayuda sirve para que otra persona este más segura
                    </p>
                </header>
                <footer className="success__footer">
                    <span className="success__footer__msg">
                        ¿Deseas ampliar la información?
                    </span>
                    <section className="success__footer__buttons">
                        <button onClick={() => navigate("/register")} className="no">No, Gracias</button>
                        <button onClick={() => setActive({
                            first: false,
                            second:true,
                            third: false
                        })} className="yes">Si, claro</button>
                    </section>
                </footer>
            </article>
            <article className={`success__page success_two ${active.second && "active"}`}>
                <span>Rellena el siguiente formulario:</span>
                <textarea name="form" id="form_textarea_to_fill"></textarea>
                <button onClick={() => setActive({
                    first: false,
                    second: false,
                    third:true
                })} className="success_two_send">Enviar</button>
            </article>
        <article className={`success__page success_three ${active.third && "active"}`}>
            <header className="success__thanks__container" >
                <img src={HAND} alt="" className="thanks__img" />
                <h1 className="thanks_title">Gracias por tu colaboración</h1>
                <span className="v" >No esperes a que te ocurra a ti tambien.</span>
            </header>
            <footer className="thanks__footer__buttons">
                <button onClick={() => navigate("/register")} className="yes">Quiero registrarme</button>
                <button className="no">No, para otra ocasión</button>
            </footer>
        </article>
        </main>
    );
};
