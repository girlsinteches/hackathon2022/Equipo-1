import React from 'react'
import LOGO from '../../media/logo.png'

export const Register = () => {
  return (
    <div className='register__container'>
        <img src={LOGO} />
        <section className='register__buttons'>
            <button>Log in</button>
            <button>Registrate</button>
        </section>
    </div>
  )
}
