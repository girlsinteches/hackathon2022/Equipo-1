import React, { useContext, useEffect, useState } from "react";
import { Navbar } from "../../navbar/navbar";
import { useForm } from "react-hook-form";
import { PositionContext } from "../../context/PositionContext";
import TRIANGLE from "./../../media/alert-triangle.png";
import { useNavigate } from "react-router-dom";

export const Alert = () => {

    const { setPositionContext } = useContext(PositionContext);
    const [position, setPosition] = useState([]);
    const [close, setClose] = useState(false)
    const [active, setActive] = useState(false);

    const { register, handleSubmit } = useForm();
    const navigate = useNavigate()

    const onSubmit = data =>{
        let dataHarass = {
            victim: "other",
            harrasment:{
                verbal: data.Verbal,
                físico: data.Físico,
                Inmobiliario: data.Inmobiliario,
                otro: data.Otro
            },
            location: position
        } 
        console.log(dataHarass);
        navigate("/success")
        
    }

    const options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0,
    };
  
    useEffect(() => {
      const success = (pos) => {
          let crd = pos.coords;
          setPosition(crd);
          setPositionContext(position);

      };
  
      const error = (err) => {
        console.warn(`ERROR(${err.code}): ${err.message}`);
      };
  
      navigator.geolocation.getCurrentPosition(success, error, options);// eslint-disable-next-line
    }, []);


    return (
        <main className={`alert__container ${close && "disappear"}`}>
            <Navbar isActive={active} closed={() => {
                setClose(true)
                navigate("/register")
                }} back={()=> setActive(!active)} />
            <article
                className={`alert__article_container ${active && "disappear"}`}
            >
                <section className="alert__title__container">
                    <img src={TRIANGLE} alt="" className="alert__title__img" />
                    <main className="alert__text">
                        <h1 className="alert__main__title">Alerta de acoso</h1>
                        <p className="alert__question">
                            ¿Quién es la persona que recibe acoso?
                        </p>
                    </main>
                    <footer className="alert__title__buttons">
                        <button>Yo</button>
                        <button onClick={() => setActive(true)}>Otra</button>
                    </footer>
                </section>
            </article>
            { active ?
            <article className={`alert__article_container`}>
                <form onSubmit={handleSubmit(onSubmit)} className="alert__form">
                    <legend htmlFor="" className="alert__question">
                        ¿Qué tipo de acoso estás presenciando?
                    </legend>
                    <ul className="alert_inputs">
                        <li>
                            <input
                                type="radio"
                                name="Verbal"
                                id="Verbal"
                                value={true}
                                {...register("Verbal")}
                            />
                            <label htmlFor="Verbal">Verbal</label>
                        </li>
                        <li>
                            <input
                                type="radio"
                                name="Físico"
                                id="Físico"
                                value={true}
                                {...register("Físico")}
                            />
                            <label htmlFor="Físico">Físico</label>
                        </li>
                        <li>
                            <input
                                type="radio"
                                name="Inmobiliario"
                                id="Inmobiliario"
                                value={true}
                                {...register("Inmobiliario")}
                            />
                            <label htmlFor="Inmobiliario">Inmobiliario</label>
                        </li>
                        <li>
                            <input
                                type="radio"
                                name="Otro"
                                id="Otro"
                                value={true}
                                {...register("Otro")}
                            />
                            <label htmlFor="Otro">Otro</label>
                        </li>
                    </ul>
                    <input id="sendLocation" type="submit" value="Enviar mi ubicación" />
                </form>
            </article>
            : ""}
        </main>
    );
};
