import React from "react";
import { useNavigate } from "react-router-dom";
import LOGO from "./../media/logo.png";

export const Navbar = ({ closed, back, isActive }) => {

    const navigate = useNavigate()

    return (
        <nav className="alert__navbar">
            <img src={LOGO} alt="" className="navbar__img" />
            <div className="navbar_button">
                {isActive && (
                    <button onClick={back}>
                        <i className="fa-solid fa-arrow-left"></i>
                    </button>
                )}
                <button onClick={closed }>
                    <i className="fa-regular fa-circle-xmark"></i>
                </button>
            </div>
        </nav>
    );
};
