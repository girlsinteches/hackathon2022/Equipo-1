const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt")
const {
    alertas,
    users,
    sequelize,
} = require("../models")

router.get("/users", async (req, res)=>{
    const payload  = await users.findAll()
    res.json(payload)
})
 

router.get("/alertas", async (req, res)=>{
    const payload = await alertas.findAll()
    res.json(payload)
})

router.post("/alerta/nueva", async (req, res) =>{
    const payload = req.body
    await alertas.create(payload)
    res.json(payload)
})


router.put("/alertas/:id", async (req, res)=>{
    const id = req.params.id
    const {tipo, longitude, latitude, hora, fecha} = req.body
    await alertas.update({
        tipo: tipo,
        longitude: longitude,
        latitude: latitude,
        hora: hora,
        fecha: fecha,
    },
    {where:{id: id}})
})


router.delete("/alertas/:id", async (req, res)=>{
    const id = req.params.id
    await alertas.destroy({
        where: {
            id: id
        },
    });
    res.json("Alerta eliminada");
})

router.post("/login", async (req,res)=>{
    const {email, password} = req.body
    const user = await users.findOne({where: {email: email}})
    if (!user){ res.json({error: "Usuario no existe"})
    return;
}
if (password == user.password)
res.json("inicio sesion")
return;
})
 

router.post("/register", async (req, res)=>{
    const {name, surname, email, telephone, age, password} = req.body
    // bcrypt.hash(password, 10).then((hash)=>{
        users.create({
            name: name,
            surname: surname,
            email:email,
            telephone: telephone,
            age: age,
            password: password
        // });
    })
    res.json("usuario creado")
})


module.exports = router;