var DataTypes = require("sequelize").DataTypes;
var _alertas = require("./alertas");
var _users = require("./users");

function initModels(sequelize) {
  var alertas = _alertas(sequelize, DataTypes);
  var users = _users(sequelize, DataTypes);


  return {
    alertas,
    users,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
